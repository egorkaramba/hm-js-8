// 1)
// function filterBy(array, dataType) {
//     return array.filter(item => typeof item !== dataType);
// }

// const stringsArray = ["travel", "hello", "eat", "ski", "lift"];

// const filteredArray = filterBy(stringsArray, 'undefined');

// const result = filteredArray.filter(str => str.length > 3).length;

// console.log(result);

// 2)
// function filterBy(array, key, value) {
//     return array.filter(item => item[key] === value);
// }

// const peopleArray = [
//     { name: "Ivan", age: 25, sex: "man" },
//     { name: "Julia", age: 30, sex: "woman" },
//     { name: "Sasha", age: 22, sex: "man" },
//     { name: "Anastasia", age: 28, sex: "woman" }
// ];

// const filteredArray = filterBy(peopleArray, 'sex', 'man');

// console.log(filteredArray);